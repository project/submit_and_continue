CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This module adds a button to forms which will submit the form, then redirect
the user back to the same form so they can carry out a new action, like:

* Add another node of the same type
* Add another menu item
* Replicate the node again with a different title (useful for testing or
  making example content)

The button names and their target routes are stored as config in YAML (take a
look here), so a configuration override can be used to alter the default
options provided by the module.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/submit_and_continue

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/submit_and_continue


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

* Install the Submit and continue module as you would normally install
  a contributed Drupal module.
  Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. This module is intentionally simple and provides no editable configuration options.
    3. To change the forms being altered, or the text of the buttons you could use
       a configuration override.


MAINTAINERS
-----------
Current maintainers:

  * Phil Wolstenholme - https://www.drupal.org/u/phil-wolstenholme
  * Ratomir Vasiljevic - https://www.drupal.org/u/ratvas
  * Volodymyr Dovhaliuk - https://www.drupal.org/u/vdovhaliuk
